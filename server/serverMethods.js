import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

Meteor.methods({
  saveConfig(userId, form) {
    check(form, Object);

    return Meteor.users.update(
      { _id: userId },
      { $set: { "profile.game": form } }
    );
  }
});
