import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
  const trusted = [
    '*.googleapis.com',
    '*.gstatic.com',
    '*.facebook.com',
    'scontent.*.fbcdn.net',
  ];

  trusted.forEach((url) => {
    url = `https://${url}`;
    BrowserPolicy.content.allowDataUrlForAll(url);
    BrowserPolicy.content.allowOriginForAll(url);
    BrowserPolicy.content.allowSameOriginForAll(url);
    BrowserPolicy.content.allowImageOrigin(url);
  });

  BrowserPolicy.content.allowOriginForAll('http://graph.facebook.com');

  ServiceConfiguration.configurations.upsert(
    { service: "facebook" },
    {
      $set: {
        appId: '1168937546479110',
        secret: 'fbaf4fceac013c8824d7e6b86f6fe724'
      }
    }
  );
});

Accounts.onCreateUser(function(options, user) {
  if (options.profile) {
    options.profile.picture = "http://graph.facebook.com/" + user.services.facebook.id + "/picture/?type=large";
    user.profile = options.profile;

    user.profile.game = {
      opener: 'Do you have a minute?',
      followup: 'Do you think I\'m cute?',
      warmer: 'Would you like to be my friend?',
      doublebind: 'I promise you\'ll like me. Let\'s get to know each other.',
      close: 'Call me maybe?',
      recovery: 'Aww. Are you sure?',
      comfortbuilder: 'I\'m a lot of fun. Get to know me a little bit and I\'ll make you laugh.',
      allin: 'Listen. I think you\'re super cute. I want to take you out.',
      goodbye: 'Give me one chance please =)',
      thankyou: 'Talk to you soon!'
    };
  }
  return user;
});
