import { composeWithTracker } from 'react-komposer';
import ProfilePic from '../ui/ProfilePic';

function composer(props, onData) {
  const handle = Meteor.user();
  if (handle) {
    const userProfilePic = handle.profile.picture;
    onData(null, { userProfilePic });
  }
}

export default composeWithTracker(composer)(ProfilePic);
