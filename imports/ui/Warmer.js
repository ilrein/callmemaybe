import React, { Component } from 'react';

import QuestionScreen from './QuestionScreen';

export default class Warmer extends Component {
  render() {
    return (
      <QuestionScreen
        question={Meteor.user().profile.game.warmer}
        yes="/close"
        no="/comfort-builder"
        maybe="/close"
      />
    );
  }
}
