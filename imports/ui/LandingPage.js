import React, { Component } from 'react';
import Spinner from 'react-spinkit';

import FacebookLogin from './FacebookLogin';

export default class LandingPage extends Component {
  constructor(props) {
    super(props);

    this.state = { loading: false };
    this.handleLogin = this.handleLogin.bind(this);
  }

  handleLogin() {
    this.setState({ loading: true });
    Meteor.loginWithFacebook((err) => {
      if (!err) {
        FlowRouter.go('/opener');
      } else {
        this.setState({ loading: false });
      }
    })
  }

  render() {
    return (
      <div className="landing-page">
        <div className="brand">
          {
            this.state.loading ?
            <Spinner
              spinnerName="cube-grid"
              noFadeIn
            /> :
            <img src="flowers.svg" />
          }
        </div>
        <FacebookLogin
          handleLogin={this.handleLogin}
        />
      </div>
    );
  }
}
