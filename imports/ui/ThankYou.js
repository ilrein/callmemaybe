import React, { Component } from 'react';

export default class ThankYou extends Component {
  render() {
    return (
      <div className="thankyou">
        <div className="body">
          <div className="body-text">
            {Meteor.user().profile.game.thankyou}
          </div>
        </div>
      </div>
    );
  }
}
