import { Meteor } from 'meteor/meteor'; // eslint-disable-line
import React, { Component } from 'react';

import { APP_NAME } from './Constants';
import AppBar from 'material-ui/AppBar';
import UserNameContainer from '../containers/UserNameContainer';
import UserProfilePictureContainer from '../containers/UserProfilePictureContainer';

// Sidebar
import Drawer from 'material-ui/Drawer';
import MenuItem from 'material-ui/MenuItem';

const style = {
  container: {
    textAlign: 'center',
    padding: 20,
  },
  center: {
    textAlign: 'center',
    fontSize: 20,
    marginBottom: 10,
  },
};

class AppNavBar extends Component {
  constructor(props) {
    super(props);

    this.state = { open: false };
    this.goToMain = this.goToMain.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.handleConfigure = this.handleConfigure.bind(this);
    this.onTouchTapLogout = this.onTouchTapLogout.bind(this);
  }

  goToMain() {
    FlowRouter.go('/opener');
    this.handleToggle();
  }

  handleConfigure() {
    FlowRouter.go('/config');
    this.handleToggle();
  }

  onTouchTapLogout() {
    Meteor.logout();
    this.handleToggle();
  }

  handleToggle() {
    this.setState({ open: !this.state.open });
  }

  render() {
    return (
      <AppBar
        className="header"
        title={APP_NAME}
        onLeftIconButtonTouchTap={this.handleToggle}
      >
        <Drawer
          className="sidebar"
          docked={false}
          width={250}
          open={this.state.open}
          onRequestChange={(open) => this.setState({ open })}
        >
          <MenuItem className="disabled-menu-item" disabled style={style.container}>
            <UserProfilePictureContainer />
          </MenuItem>
          <MenuItem focusState="none" disabled style={style.center}>
            <UserNameContainer />
          </MenuItem>
          <MenuItem focusState="none" className="menu-item" onTouchTap={this.goToMain}>
            Back to Start
          </MenuItem>
          <MenuItem focusState="none" className="menu-item" onTouchTap={this.handleConfigure}>
            Configure
          </MenuItem>
          <MenuItem focusState="none" className="menu-item" onTouchTap={this.onTouchTapLogout}>
            Logout
          </MenuItem>
        </Drawer>
      </AppBar>
    );
  }
}

export default AppNavBar;
