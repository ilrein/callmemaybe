import React, { Component } from 'react';

import QuestionScreen from './QuestionScreen';

export default class Recovery extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <QuestionScreen
        question={Meteor.user().profile.game.recovery}
        yes="/goodbye"
        no="/comfort-builder"
        maybe="/double-bind"
      />
    );
  }
}
