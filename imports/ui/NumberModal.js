import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import TextField from 'material-ui/TextField';
import { deepPurple100, deepPurple300 } from 'material-ui/styles/colors';

export default class NumberModal extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const actions = [
      <FlatButton
        label="Save"
        primary
        onTouchTap={this.props.savePhoneNumber}
      />,
    ];

    return (
      <Dialog
        title="Contact Info"
        actions={actions}
        modal={false}
        open={this.props.open}
        onRequestClose={this.props.handleToggle}
      >
        <TextField
          name="name"
          onChange={this.props.cacheDisplayName}
          hintText="Mary Jane"
          type="text"
          underlineStyle={{ borderColor: deepPurple100 }}
          underlineFocusStyle={{ borderColor: deepPurple300 }}
        />
        <TextField
          name="phone-number"
          hintText="647 828 9999"
          onChange={this.props.cacheNumber}
          type="text"
          underlineStyle={{ borderColor: deepPurple100 }}
          underlineFocusStyle={{ borderColor: deepPurple300 }}
        />
      </Dialog>
    );
  }
}
