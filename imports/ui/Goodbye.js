import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';

export default class Goodbye extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div className="dashboard">
        <div className="body">
          <div className="body-text">
            {Meteor.user().profile.game.goodbye}
          </div>
        </div>
        <div className="actions">
          <RaisedButton
            label="Maybe"
            fullWidth
            style={{ height: 60, margin: '10px 0' }}
            onTouchTap={() => FlowRouter.go('/double-bind')}
          />
        </div>
      </div>
    );
  }
}
