import React, { Component } from 'react';

import LandingPage from './LandingPage';

export default class Home extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="home">
        <LandingPage />         
      </div>
    );
  }
}
