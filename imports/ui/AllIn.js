import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';

import QuestionScreen from './QuestionScreen';

export default class AllIn extends Component {
  render() {
    return (
      <div className="dashboard">
        <div className="body">
          <div className="body-text">
            {Meteor.user().profile.game.allin}
          </div>
        </div>
        <div className="actions">
          <RaisedButton
            label="Yes"
            fullWidth
            style={{ height: 60, margin: '10px 0' }}
            onTouchTap={() => FlowRouter.go('/close')}
          />
          <RaisedButton
            label="Maybe"
            fullWidth
            style={{ height: 60, margin: '10px 0' }}
            onTouchTap={() => FlowRouter.go('/close')}
          />
        </div>
      </div>
    );
  }
}
