import React, { Component } from 'react';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { deepPurple400, deepPurple500 } from 'material-ui/styles/colors';

export default class Config extends Component {
  constructor(props) {
    super(props);

    this.state = { open: false };
    this.saveConfig = this.saveConfig.bind(this);
    this.handleClose = this.handleClose.bind(this);
  }

  handleClose() {
    this.setState({ open: !this.state.open });
    FlowRouter.go('/opener');
  }

  saveConfig() {
    const form = {};
    for (const key in this.refs) {
      if (this.refs.hasOwnProperty(key)) {
        const value = this.refs[key].getValue();
        form[key] = value;
      }
    }

    Meteor.call('saveConfig', Meteor.userId(), form, (err, res) => {
      if (!err) {
        this.setState({ open: true });
      }
    });
  }

  render() {
    const game = Meteor.user().profile.game;
    return (
      <section className="config">
        <Dialog
         title="Saved"
         actions={
           <RaisedButton
             label="OK"
             backgroundColor={deepPurple500}
             labelColor="#FFF"
             fullWidth
             style={{ width: '100%' }}
             onTouchTap={this.handleClose}
           />
         }
         modal={false}
         open={this.state.open}
         onRequestClose={this.handleClose}
       >
         Updated your game!
       </Dialog>
        <div className="form-item">
          <label>Opener</label>
          <TextField
            hintText={game.opener}
            hintStyle={{ color: deepPurple400 }}
            inputStyle={{ color: deepPurple400 }}
            defaultValue={game.opener}
            fullWidth
            underlineFocusStyle={{ borderColor: deepPurple500 }}
            ref="opener"
          />
        </div>
        <div className="form-item">
          <label>Follow Up</label>
          <TextField
            hintText={game.followup}
            hintStyle={{ color: deepPurple400 }}
            inputStyle={{ color: deepPurple400 }}
            defaultValue={game.followup}
            fullWidth
            underlineFocusStyle={{ borderColor: deepPurple500 }}
            ref="followup"
          />
        </div>
        <div className="form-item">
          <label>Double Bind</label>
          <TextField
            hintText={game.doublebind}
            hintStyle={{ color: deepPurple400 }}
            inputStyle={{ color: deepPurple400 }}
            defaultValue={game.doublebind}
            fullWidth
            underlineFocusStyle={{ borderColor: deepPurple500 }}
            ref="doublebind"
          />
        </div>
        <div className="form-item">
          <label>Warmer</label>
          <TextField
            hintText={game.warmer}
            hintStyle={{ color: deepPurple400 }}
            inputStyle={{ color: deepPurple400 }}
            defaultValue={game.warmer}
            fullWidth
            underlineFocusStyle={{ borderColor: deepPurple500 }}
            ref="warmer"
          />
        </div>
        <div className="form-item">
          <label>Close</label>
          <TextField
            hintText={game.close}
            hintStyle={{ color: deepPurple400 }}
            inputStyle={{ color: deepPurple400 }}
            defaultValue={game.close}
            fullWidth
            underlineFocusStyle={{ borderColor: deepPurple500 }}
            ref="close"
          />
        </div>
        <div className="form-item">
          <label>Recovery</label>
          <TextField
            hintText={game.recovery}
            hintStyle={{ color: deepPurple400 }}
            inputStyle={{ color: deepPurple400 }}
            defaultValue={game.recovery}
            fullWidth
            underlineFocusStyle={{ borderColor: deepPurple500 }}
            ref="recovery"
          />
        </div>
        <div className="form-item">
          <label>Comfort Builder</label>
          <TextField
            hintText={game.comfortbuilder}
            hintStyle={{ color: deepPurple400 }}
            inputStyle={{ color: deepPurple400 }}
            defaultValue={game.comfortbuilder}
            fullWidth
            underlineFocusStyle={{ borderColor: deepPurple500 }}
            ref="comfortbuilder"
          />
        </div>
        <div className="form-item">
          <label>All In</label>
          <TextField
            hintText={game.allin}
            hintStyle={{ color: deepPurple400 }}
            inputStyle={{ color: deepPurple400 }}
            defaultValue={game.allin}
            fullWidth
            underlineFocusStyle={{ borderColor: deepPurple500 }}
            ref="allin"
          />
        </div>
        <div className="form-item">
          <label>Goodbye</label>
          <TextField
            hintText={game.goodbye}
            hintStyle={{ color: deepPurple400 }}
            inputStyle={{ color: deepPurple400 }}
            defaultValue={game.goodbye}
            fullWidth
            underlineFocusStyle={{ borderColor: deepPurple500 }}
            ref="goodbye"
          />
        </div>
        <RaisedButton
          label="Save"
          backgroundColor={deepPurple500}
          labelColor="#FFF"
          fullWidth
          className="save-config"
          onTouchTap={this.saveConfig}
        />
      </section>
    );
  }
}
