import React, { Component } from 'react';

import QuestionScreen from './QuestionScreen';

export default class ComfortBuilder extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <QuestionScreen
        question={Meteor.user().profile.game.comfortbuilder}
        yes="/close"
        no="/all-in"
        maybe="/close"
      />
    );
  }
}
