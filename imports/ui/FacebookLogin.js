import { Meteor } from 'meteor/meteor'
import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton';
import { fullWhite } from 'material-ui/styles/colors';

export default class FacebookLogin extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <section>
        <FlatButton
          backgroundColor="#fff"
          hoverColor="#eee"
          label="Login with Facebook"
          fullWidth
          onTouchTap={this.props.handleLogin}
        />
      </section>
    );
  }
}
