import React, { Component } from 'react';

import QuestionScreen from './QuestionScreen';

class Opener extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <QuestionScreen
        question={Meteor.user().profile.game.opener}
        yes="/followup"
        no="/recovery"
        maybe="/followup"
      />
    );
  }
}

export default Opener;
