import React, { Component } from 'react';
import Avatar from 'material-ui/Avatar';

export default class ProfilePic extends Component {
  render() {
    return (
      <Avatar
        src={this.props.userProfilePic}
        size={150}
      />
    );
  }
}
