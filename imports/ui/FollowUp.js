import React, { Component } from 'react';

import QuestionScreen from './QuestionScreen';

export default class FollowUp extends Component {
  render() {
    return (
      <QuestionScreen
        question={Meteor.user().profile.game.followup}
        yes="/warmer"
        no="/recovery"
        maybe="/warmer"
      />
    );
  }
}
