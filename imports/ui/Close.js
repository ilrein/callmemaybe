import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';

import QuestionScreen from './QuestionScreen';
import NumberModal from './NumberModal';

export default class Close extends Component {
  constructor(props) {
    super(props);

    this.state = {
      numberModalShowing: false,
      phoneNumber: null,
      displayName: null,
      familyName: null,
    };

    this.cacheNumber = this.cacheNumber.bind(this);
    this.cacheDisplayName = this.cacheDisplayName.bind(this);
    this.savePhoneNumber = this.savePhoneNumber.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
  }

  cacheNumber(e, phoneNumber) {
    this.setState({ phoneNumber });
  }

  cacheDisplayName(e) {
    this.setState({ displayName: e.target.value });
  }

  savePhoneNumber() {
    this.handleToggle();

    // create a new contact object
    let contact = navigator.contacts.create();
    contact.displayName = `${this.state.displayName}`;
    contact.nickname = `${this.state.displayName}`;

    // populate some fields
    let name = new ContactName();
    const nameString = this.state.displayName.split(' ');
    name.givenName = nameString[0];
    name.familyName = nameString[1];
    contact.name = name;

    // using some objects that work with cordova
    const phoneNumbers = [];
    const number = new ContactField('mobile', this.state.phoneNumber, true);
    phoneNumbers.push(number);
    contact.phoneNumbers = phoneNumbers;

    // save to device
    contact.save(
      (contact) => {
        navigator.notification.confirm(
          'Let\'s take a selfie together?',
          (buttonIndex) => {
            // OK
            if (buttonIndex === 1) {
              // save the selfie here
              navigator.camera.getPicture(
                (imageURI) => {
                  navigator.notification.alert(
                    'Image saved.',
                    () => { FlowRouter.go('/thankyou') }
                  )
                },
                (error) => alert(`Camera error: ${error}`),
                {
                  destinationType: Camera.DestinationType.FILE_URI,
                  saveToPhotoAlbum: true,
                  correctOrientation: true
                }
              )
            } else {
              navigator.notification.alert(
                'Maybe next time.',
                () => { FlowRouter.go('/thankyou') }
              )
            }
          },
          'Saved',
          ['OK', 'Cancel']
        );
      },
      (err) => {
        alert('err', err);
      }
    );
    return;
  }

  handleToggle() {
    this.setState({
      numberModalShowing: !this.state.numberModalShowing
    });
  }

  render() {
    return (
      <div className="dashboard">
        <NumberModal
          open={this.state.numberModalShowing}
          cacheNumber={this.cacheNumber}
          cacheDisplayName={this.cacheDisplayName}
          handleToggle={this.handleToggle}
          savePhoneNumber={this.savePhoneNumber}
        />
        <div className="body">
          <div className="body-text">
            {Meteor.user().profile.game.close}
          </div>
        </div>
        <div className="actions">
          <RaisedButton
            label="OK"
            fullWidth
            style={{ height: 60, margin: '10px 0' }}
            onTouchTap={this.handleToggle}
          />
          {/*<RaisedButton
            label="Facebook"
            fullWidth
            style={{ height: 60, margin: '10px 0' }}
          />
          <RaisedButton
            label="Instagram"
            fullWidth
            style={{ height: 60, margin: '10px 0' }}
          />*/}
        </div>
      </div>
    );
  }
}
