import React, { Component } from 'react';
import RaisedButton from 'material-ui/RaisedButton';

export default class QuestionScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div className="dashboard">
        <div className="body">
          <div className="body-text">
            {this.props.question}
          </div>
        </div>
        <div className="actions">
          <RaisedButton
            label="Yes"
            fullWidth
            style={{ height: 60, margin: '10px 0' }}
            onTouchTap={() => FlowRouter.go(this.props.yes)}
          />
          <RaisedButton
            label="No"
            fullWidth
            style={{ height: 60, margin: '10px 0' }}
            onTouchTap={() => FlowRouter.go(this.props.no)}
          />
          <RaisedButton
            label="Maybe"
            fullWidth
            style={{ height: 60, margin: '10px 0' }}
            onTouchTap={() => FlowRouter.go(this.props.maybe)}
          />
        </div>
      </div>
    );
  }
}
