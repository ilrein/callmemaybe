// Main rendering libraries
import React from 'react';
import { mount } from 'react-mounter';
import { Tracker } from 'meteor/tracker'; // eslint-disable-line

// Layouts
import MainLayout from '../imports/layouts/MainLayout';
import BasicLayout from '../imports/layouts/BasicLayout';

// Pages
import Home from '../imports/ui/Home';
import Register from '../imports/ui/Register';
import Config from '../imports/ui/Config';

// App screens
import Opener from '../imports/ui/Opener';
import FollowUp from '../imports/ui/FollowUp';
import DoubleBind from '../imports/ui/DoubleBind';
import Warmer from '../imports/ui/Warmer';
import Close from '../imports/ui/Close';
import Recovery from '../imports/ui/Recovery';
import ComfortBuilder from '../imports/ui/ComfortBuilder';
import AllIn from '../imports/ui/AllIn';
import Goodbye from '../imports/ui/Goodbye';
import ThankYou from '../imports/ui/ThankYou';

// Needed for onTouchTap
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

//-------
// Routes
//-------

// Home route
FlowRouter.route('/', {
  action() {
    mount(BasicLayout, {
      content: (<Home />),
    });
  },
});

// Register route
FlowRouter.route('/register', {
  action() {
    mount(BasicLayout, {
      content: (<Register />),
    });
  },
});

// Opener route
FlowRouter.route('/opener', {
  action() {
    mount(MainLayout, {
      content: (<Opener />),
    });
  },
});

// Follow Up
FlowRouter.route('/followup', {
  action() {
    mount(MainLayout, {
      content: (<FollowUp />),
    });
  },
});

// Warmer
FlowRouter.route('/warmer', {
  action() {
    mount(MainLayout, {
      content: (<Warmer />),
    });
  },
});

// DoubleBind
FlowRouter.route('/double-bind', {
  action() {
    mount(MainLayout, {
      content: (<DoubleBind />),
    });
  },
});

// Close
FlowRouter.route('/close', {
  action() {
    mount(MainLayout, {
      content: (<Close />),
    });
  },
});

// Recovery
FlowRouter.route('/recovery', {
  action() {
    mount(MainLayout, {
      content: (<Recovery />),
    });
  },
});

// Comfort Builder
FlowRouter.route('/comfort-builder', {
  action() {
    mount(MainLayout, {
      content: (<ComfortBuilder />),
    });
  },
});

// All in
FlowRouter.route('/all-in', {
  action() {
    mount(MainLayout, {
      content: (<AllIn />),
    });
  },
});

// Goodbye
FlowRouter.route('/goodbye', {
  action() {
    mount(MainLayout, {
      content: (<Goodbye />),
    });
  },
});

// ThankYou
FlowRouter.route('/thankyou', {
  action() {
    mount(MainLayout, {
      content: (<ThankYou />),
    });
  },
});

// Config
FlowRouter.route('/config', {
  action() {
    mount(MainLayout, {
      content: (<Config />),
    });
  },
});

// Force login page if not authenticated
Tracker.autorun(() => {
  if (!Meteor.userId()) {
    FlowRouter.go('/');
  }
});
