App.accessRule('*');

// This section sets up some basic app metadata,
// the entire section is optional.
App.info({
  id: 'com.callmemaybe.ilia.reingold',
  name: 'CallMeMaybe',
  description: 'Helping introverts close digits.',
  author: 'Ilia Reingold',
  email: 'ilia.reingold@gmail.com',
  website: 'http://callmemaybe.io'
});

// Set up resources such as icons and launch screens.
App.icons({
  // 'iphone': 'icons/icon-60.png',
  // 'iphone_2x': 'icons/icon-60@2x.png'
});

App.launchScreens({
  // 'iphone': 'splash/Default~iphone.png',
  // 'iphone_2x': 'splash/Default@2x~iphone.png'
});

// Set PhoneGap/Cordova preferences
// App.setPreference('BackgroundColor', '0xff0000ff');
// App.setPreference('HideKeyboardFormAccessoryBar', true);
// App.setPreference('Orientation', 'default');
// App.setPreference('Orientation', 'all', 'ios');
