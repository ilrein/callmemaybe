- I really think we could be friends. Would you like to be my friend?
- Can we be friends?
- Would you like to hang out with me sometime?

# Public IP
52.42.106.85

ns-842.awsdns-41.net.
ns-134.awsdns-16.com.
ns-1138.awsdns-14.org.
ns-2037.awsdns-62.co.uk.

---

App.icons(icons)
Valid key values:

iphone_2x (120x120)
iphone_3x (180x180)
ipad (76x76)
ipad_2x (152x152)
ipad_pro (167x167)
ios_settings (29x29)
ios_settings_2x (58x58)
ios_settings_3x (87x87)
ios_spotlight (40x40)
ios_spotlight_2x (80x80)
android_mdpi (48x48)
android_hdpi (72x72)
android_xhdpi (96x96)
android_xxhdpi (144x144)
android_xxxhdpi (192x192)


App.launchScreens(launchScreens)
Valid key values:

iphone_2x (640x960)
iphone5 (640x1136)
iphone6 (750x1334)
iphone6p_portrait (1242x2208)
iphone6p_landscape (2208x1242)
ipad_portrait (768x1024)
ipad_portrait_2x (1536x2048)
ipad_landscape (1024x768)
ipad_landscape_2x (2048x1536)
android_mdpi_portrait (320x470)
android_mdpi_landscape (470x320)
android_hdpi_portrait (480x640)
android_hdpi_landscape (640x480)
android_xhdpi_portrait (720x960)
android_xhdpi_landscape (960x720)
android_xxhdpi_portrait (1080x1440)
android_xxhdpi_landscape (1440x1080)
